# Rust

## History

- Developed by Mozilla Research in 2010
	- Goal: Servo
- AND the rust community!
- v1.0.0 in 2015
- next LTS Sept. 2018

## Problems of C/C++

- Dangling pointers - have a ptr to sth that does not exist anymore
- iterator invalidation - like ConcurrentModificationException
- thread safety - accidental concurrent access
- ecosystem - ... cmake

=> With great power comes great responsibility
=> BUT: Runtime errors (SEGFAULTS)

## So what is Rust?

Rust is a low level (systems) programming language with high level language convenience

Hack without fear (the compiler will stop you)

Rust is a systems programming language that runs blazingly fast, prevents almost all crashes, and eliminates data races.

	fn main() {
		println!("hello {}", "world!");
	}

rust playpen!!

## Rust goals

### From another presentation

- modern idioms
	- immutable by default
	- no null pointers
	- zero cost abstractions
	- pivate by default
- strong ownership model
	- move semantics -- think affine types
	- pass by value (which includes references)
- memory safe without garbage collection
	- library provided smart pointers
	- Either have unique ownership and mutability Or shared ownership and immutability
- Hygenic macros
	- type-safe meta programming
- predictability
	- no implicit language (e.g. implicit casting)
	- readable grammar, functional design, nouns and verbs
	- pwoerful pattern matching, enums
	- explicit what makes sence
	- help compiler understand you code
	- no GC
- zero cost abstractions
	- monomorphization
	- variadic enums (Option, Result)
	- newtype patterns
	- zero overhead FFI (very fast)
	- most of the time as fast as C

### Stuff from one presentation

- safety
	- memory safety
		- no leaks
		- no use after free
		- no buffer overflow?
		- no double free
		- no null dereferencing
	- datarace safety
		- does not prevent deadlocks
		- compiler "knows" about thread and can track object passed and used between threads
			- mutable shared state only accessible by one thread at a time
- tools
	- Cargo
	- Clippy
	- Rustfmt
	- RLS + racer
- great documentation
	- book
	- rustonomicon (dark arts)
	- doc.rs
	- crates.io
- stability
	- stability as a deliverable
	- LTS, stable, beta, nightly
	- openRFC
	- 6 week release
	- stability guarantees guarantee breaking old code
		- created big nightly-stable diverge
		- feature guards
- POWERFUL libraries
	- enable libraries to extend core functionality without overhead
	- trivial to include libs
	- easy to build
- community
	- forums
	- open development
	- open community etc
- PREDICTABLE

### Language itself

- expression langauge
- strongly typed
- module system

### very simplistic

- no overhead
- no crashes :D
- ecosystem and community
- easy to learn

### also small

- iterators
- concurrency
- ownership
- lifetimes
- error handling
- variadic enums
- no null

## Who uses rust

- Common areas:
    - Systems programming, operating systems, security, networking, data processing, AI, game engines
- Some names:
    - Firefox, Gecko, Servo
    - Canoncial (Ubuntu)
    - Microsoft Azure
    - Dropbox
    - Npm
    - Couersa
    - Threema messenger apps
    - Chef - continuous automation
    - Atlassian
    - Tilde - Performance analyis tools for ruba apps
    - Tor project - rust rewrite as of 2017
    - a LOT of open source projects, webassembly

## C interoperability

- cbindgen => generate c headers for rust library
- bindgen => generate rust extern defintion for a c library
- easy transition - incremental rewrites (if at all)

## Special

- semicolons
- match
- variadic enums
- traits and impl
- smart pointers and Heap allocations
- destructing
- immutable, mutability
- Generics and Trait objects
- move semantics (ownership) and borrowing
- mutability and ownership in function signatures!!
- macros
- unsafe == break the rules, tell the compiler about it and guarantee it
- other
	- thread safety
	- powerful zero cost abstractions
	- error handling (no exceptions)
	- associated types, constants
	- closures
	- plugin ecosystem
	- labraries
	- tuples
	- coroutines
	- easy to embed rust into other languages
		- python, ruby, c, c++

## Other

- Data oriented
- trait system, no OO
- no performance loss - as long as you stay idiomatic
- high performance through rust and js
	- because rust can be adapted and wrapped around invariants and enforce them
- Servo, Redox, Rust postgres or the other lib for sql queries (like kotlin thing for android)
- EVERYONE moves to rust
	- from JS to C
- Positive impact on other languages
    - Wasm toolchain
    - LLVM commits (coroutines)

## IDEs

- Intellij-Rust
- universal - Rust Language Server
- VS code

## Evaluation

Pro

- Performance
- Safety
- Lower maintenance
- Expand # of maintainers
	- more ppl can write production ready in rust than in c

Con

- the compiler is your worst enemy AND friend (TODO show some error)
	- makes fast drafting/scripting impossible when you dont know what your doing
- jobs
- hypes
- for fun?

## Problems (Worked on)

- still a systems programming language
- GUI - no OO designs possible => but solves Performance (2018 talk)

## Resources

- into_rust
- rust-by-example - use in presentation
- rust play pen - use in presentation
- rust book
- rustonomicon
- this week in rust
- exercism
- ...

## Ownership

- The right to destroy
- There can only be one

## Borrowing

## Mutability

- immutable default

## Lieftimes
