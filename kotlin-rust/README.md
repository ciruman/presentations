[![pipeline status](https://gitlab.com/ciruman/presentations/badges/master/pipeline.svg)](https://gitlab.com/ciruman/presentations/commits/master)

# Presentations about Kotlin and Rust
In order to generate the html page run the following commands:

```
node asciidoctor-revealjs-kotlin.js
node asciidoctor-revealjs-rust.js 
```

More information about asciidoctor-revealjs:

[Asciidocor-revealjs documentation](https://asciidoctor.org/docs/asciidoctor-revealjs/)

The presentations will be available here(**updated automatically after each change**):

[Rust Presentation](https://ciruman.gitlab.io/presentations/rust.html)

[Kotlin Presentation](https://ciruman.gitlab.io/presentations/kotlin.html)

The keyboard shortcuts are:
- <kbd>N</kbd>, <kbd>SPACE</kbd>:	Next slide
- <kbd>P</kbd>: Previous slide
- <kbd>←</kbd>, <kbd>H</kbd>: Navigate left
- <kbd>→</kbd>, <kbd>L</kbd>: Navigate right
- <kbd>↑</kbd>, <kbd>K</kbd>: Navigate up
- <kbd>↓</kbd>, <kbd>J</kbd>: Navigate down
- <kbd>Home</kbd>: First slide
- <kbd>End</kbd>: Last slide
- <kbd>B</kbd>, <kbd>.</kbd>: Pause (Blackout)
- <kbd>F</kbd>: Fullscreen
- <kbd>ESC</kbd>, <kbd>O</kbd>: Slide overview / Escape from full-screen
- <kbd>S</kbd>: Speaker notes view
- <kbd>?</kbd>: Show keyboard shortcuts
- <kbd>alt</kbd> + click: Zoom in. Repeat to zoom back out.